import java.util.ArrayList;

/**
 * Acts as an intermediary between the campervan rental system backend and the depots
 * Handles bookings with the depots, and hands new vans over to the depots
 * Takes the the commands from VanRentalSystemBackend, and accesses its ArrayList<Depot> to interact with the Depots
 * @author Justin Ng
 *
 */
public class HeadOffice {
	
	private ArrayList<Depot> depots = new ArrayList<Depot>();
	private ArrayList<Integer> bookingIDs = new ArrayList<Integer>();

	
	/**
	 * Just a constructor to create a new headOffice; doesn't need any inputs
	 */
	public HeadOffice() {
	}
	
	/**
	 * Takes the inputs for the requested bookings, and asks the depots
	 *   for how many vans they can give for the time slot
	 * Once ascertained there are enough, it asks the depots to assign as many
	 *   vans as it can, and receives a string holding the location and which vans
	 *   were assigned; this is all constructed into one string
	 * If there aren't enough, then it returns the appropriate denial
	 * @param ID            the ID of the booking request 
	 * @param auto          the number of requested automatic vehicles
	 * @param manual        the number of requested manual vehicles
	 * @param reqSlot       the time slot (start / end) requested
	 * @return returnString either a string holding the locations and vehicles if successful,
	 *                      or "Booking rejected " if not
	 */
	public String newBooking(int ID, int auto, int manual, TimeSlot reqSlot) {
		if (bookingIDs.contains((Integer) ID) || (auto == 0 && manual == 0)) {
			return "rejected";
		}
		
		int[] numAvailable = {0,0}; // index 0 is number of Auto, index 1 is number of Manual
		int[] numRequested = {auto, manual};
		String returnString = ID + " ";
		
		// check the number of available vans for each type in the time slot requested
		for	(Depot depot : depots) {
			depot.getAvailable(numAvailable, auto, manual, reqSlot);
			if (numAvailable[0] >= auto && numAvailable[1] >= manual) {
				break;
			}
		}
		
		// has enough available vans for each type in the time slot requested, so assign
		if (numAvailable[0] >= auto && numAvailable[1] >= manual) {
			for	(Depot depot : depots) {
				if (numRequested[0] == 0 && numRequested[1] == 0) {
					break;
				}
				returnString += depot.assignVans(ID, numRequested, reqSlot);
			}
			bookingIDs.add(ID);
			returnString = returnString.substring(0, returnString.length() - 2);
		} else { // not enough available vans for each type in the time slot requested, so don't assign
			returnString = "rejected";
		}
		return returnString;
	}
	
	/**
	 * Takes the input ID for the requested booking deletion, and checks
	 *   the booking exists.
	 * Once ascertained that it exists, it asks each depot to delete
	 *   the bookings associated to the ID
	 * If the booking doesn't exist, then it returns the appropriate denial.
	 * @param ID            the ID of the booking to be cancelled
	 * @return returnString has either "Cancel ID" or "Cancel rejected"
	 *                        depending on if the cancellation was successful
	 */
	public String cancelBooking(int ID) {
		String returnString = "";
		// checks if the booking exists, and if it does, delete, else return denial
		if (bookingIDs.contains((Integer) ID)) {
			bookingIDs.remove((Integer) ID);
			for (Depot depot : depots) {
				depot.unassignVans(ID);
			}
			returnString = Integer.toString(ID);
		} else {
			returnString = "rejected";
		}
		
		return returnString;
	}
	
	/**
	 * Takes the inputs for the requested booking change, and asks the
	 *   depots for how many vans they can give / reassign for the time slot
	 * Once ascertained there are enough, it runs the cancelBooking() and
	 *   newBooking() functions to cancel the original booking, and reassign
	 *   vans.
	 * If there aren't enough, then it returns the appropriate denial
	 * @param ID            the ID of the booking change request 
	 * @param auto          the number of requested automatic vehicles
	 * @param manual        the number of requested manual vehicles
	 * @param reqSlot       the time slot (start / end) requested
	 * @return returnString either a string holding the locations and vehicles if successful,
	 *                        or "Change rejected " if not
	 */
	public String changeBooking(int ID, int auto, int manual, TimeSlot reqSlot) {
		if (!bookingIDs.contains((Integer) ID) || (auto == 0 && manual == 0)) {
			return "rejected";
		}
		int[] numAvailable = {0,0}; // index 0 is number of Auto, index 1 is number of Manual
		String returnString = "";
		
		// check the number of available vans for each type in the time slot requested
		for	(Depot depot : depots) {
			if (numAvailable[0] >= auto && numAvailable[1] >= manual) {
				break;
			}
			depot.getChangeable(ID, numAvailable, auto, manual, reqSlot);
		}
		
		// has enough available vans for each type in the time slot requested, so cancel old booking, create new booking
		// if not then set up appropriate denial
		if (numAvailable[0] >= auto && numAvailable[1] >= manual) {
			cancelBooking(ID);
			returnString = newBooking(ID, auto, manual, reqSlot);
			return returnString;
		} else {
			returnString = "rejected";
		}
		
		return returnString;
	}
	
	/**
	 * Takes the inputs for the requested location to print, and asks the
	 *   depot for a list of all its bookings.
	 * @param location      the location of the depot to have its bookings printed
	 * @return returnString string that contains all the bookings of the location
	 *                        as returned from the depot
	 */
	public String locationBookingPrinter(String location) {
		// gets the depot object to be operated on
		Depot tempDepot = new Depot(location);
		String returnString = "";
		if (depots.contains(tempDepot)) {
			Depot printedDepot = depots.get(depots.indexOf(tempDepot));
			
			// gets the string from the depot
			returnString = printedDepot.bookingPrinter();
		}
		
		return returnString;
		
	}
	
	/**
	 * Instructs the depot of the specified location to construct a new van with the
	 *   the given requirements, taking the specifications and passing it onto Depot
	 * @param location     the location of the depot to get a new van
	 * @param name         the name of the new van
	 * @param transmission the transmission of the new van
	 */
	public void newVanForDepot(String location, String name, String transmission) {
		Depot tempDepot = new Depot(location);
		// it the depot doesn't exist, create it
		if (!depots.contains(tempDepot)) {
			depots.add(tempDepot);
		}
		
		// gets the depot object to be operated on, and sends the specifications of
		//   the new van to the depot 
		Depot requestedDepot = depots.get(depots.indexOf(tempDepot));
		requestedDepot.acceptNewVan(location, name, transmission);
	}
}
