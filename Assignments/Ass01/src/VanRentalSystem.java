import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Runs the campervan rental system
 * It takes the file, and inputs it into the VanRentalSystemBackend
 * @author Justin Ng
 *
 */
public class VanRentalSystem {
	
	static VanRentalSystemBackend backend = new VanRentalSystemBackend();
	
	/**
	 * Takes the file, opens it, and passes it onto VanRentalSystemBackend to run parse and run the commands.
	 * Once VanRentalSystem returns, it closes the file.
	 * @param args contains the arguments of the command line input; args[0] contains the file itself.
	 */
	public static void main(String[] args) {
		Scanner fileInput = null;
		try {
			fileInput = new Scanner(new FileReader(args[0]));
			backend.runCommands(fileInput);
			
		} catch (FileNotFoundException e) {
			
		} finally {
			if (fileInput != null) {
				fileInput.close();
			}
		}
	}
}
