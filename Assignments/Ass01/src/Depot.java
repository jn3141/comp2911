import java.util.ArrayList;

/**
 * Acts as an intermediary between the head office and the campervans
 * Handles the vans themselves, taking the the commands from HeadOffice,
 *   and assigns vans / checks their availabilities
 * @author Justin Ng
 *
 */
public class Depot {
	
	private String location;
	private ArrayList<Campervan> campervans = new ArrayList<Campervan>();
	
	/**
	 * Constructor for a new depot
	 * @param location the location of the depot
	 */
	public Depot(String location) {
		this.location = location;
	}
	
	/**
	 * Creates a new campervan, and adds it to the ArrayList<Campervan> campervans
	 * @param name         name of the campervan
	 * @param transmission transmission of the campervan
	 * @pre   campervans.contains(newCampervan) = false
	 * @post  campervans.contains(newCampervan) = true
	 */
	public void acceptNewVan(String location, String name, String transmission) {
		Campervan newCampervan = new Campervan(location, name, transmission);
		campervans.add(newCampervan);
    }
	
	/**
	 * Finds which vans are available to be assigned directly for the time slot
	 * Returns this value in the passed in array so it can be used across multiple
	 *   depots
	 * @param numAvailable[] an array used across multiple depots to declare upwards
	 *                         how many vans are available for auto and manual; index
	 *                         0 is for auto, index 1 is for manual
	 * @param reqSlot        the time slot requested for the booking
	 * @pre   numAvailable[] does not include the number of available vans at this depot
	 * @post  numAvailable[] includes the number of available vans at this depot, and enough
	 * 						   to satisfy the requirement if enough
	 */
	public void getAvailable(int[] numAvailable, int auto, int manual, TimeSlot reqSlot) {
		for (Campervan campervan : campervans) {
			// checks across the vans for which automatics / manuals are available for the time slot
			if (campervan.isAutomatic() && campervan.isAvailable(reqSlot)) {
				numAvailable[0]++;
			} else if (campervan.isManual() && campervan.isAvailable(reqSlot)){
				numAvailable[1]++;
			}
			
			if (numAvailable[0] >= auto && numAvailable[1] >= manual) {
				break;
			}
		}
	}
	
	/**
	 * Finds which vans are available to be assigned directly or reassigned for 
	 *   the given ID for the given time slot
	 * Returns this value in the passed in array so it can be used across multiple
	 *   depots
	 * @param ID             the booking ID associated with the booking to be changed
	 * @param numAvailable[] an array used across multiple depots to declare upwards
	 *                         how many vans are available for auto and manual; index
	 *                         0 is for auto, index 1 is for manual
	 * @param reqSlot        the time slot requested for the booking change
	 * @pre   numAvailable[] does not include the number of available vans at this depot
	 * @post  numAvailable[] includes the number of available vans at this depot, and enough
	 * 						   to satisfy the requirement if enough
	 */
	public void getChangeable(int ID, int[] numAvailable, int auto, int manual, TimeSlot reqSlot) {
		for (Campervan campervan : campervans) {
			if (campervan.isAutomatic() && (campervan.isAvailableForChange(ID, reqSlot))) {
				numAvailable[0] += 1;
			} else if (campervan.isManual() && (campervan.isAvailableForChange(ID, reqSlot))) {
				numAvailable[1] += 1;
			}
			
			if (numAvailable[0] >= auto && numAvailable[1] >= manual) {
				break;
			}
		}
	}
	
	/**
	 * Assigns vans that are available for the given time slot
	 * Returns a string that denotes which vans were assigned
	 * @param ID             ID of the booking
	 * @param numRequested[] an array used across multiple depots to track upwards
	 *                         how many vans that are requested are left to be fulfilled;
	 *                         index 0 is for auto, index 1 is for manual
	 * @param reqSlot        the time slot requested for the booking change
	 * @return returnString  either a string holding the locations and vehicles if successful,
	 *                         or an empty string otherwise
	 */
	public String assignVans(int ID, int[] numRequested, TimeSlot reqSlot) {
		boolean firstOfLocation = true;
		String returnString = "";
		for (Campervan campervan : campervans) {
			if (numRequested[0] > 0 && campervan.isAutomatic() && campervan.isAvailable(reqSlot)) {
				campervan.setBooking(ID, reqSlot);
				numRequested[0]--;
				
				// first van of the location, so don't include ","
				if (firstOfLocation) {
					firstOfLocation = false;
					returnString = returnString + location + " " + campervan.getName();
				} else { // not the first van of the location, so include ","
					returnString += ", " + campervan.getName();
				}
				
			} else if (numRequested[1] > 0 && campervan.isManual() && campervan.isAvailable(reqSlot)) {
				campervan.setBooking(ID, reqSlot);
				numRequested[1]--;
				
				// first van of the location, so don't include ","
				if (firstOfLocation) {
					firstOfLocation = false;
					returnString = returnString + location + " " + campervan.getName();
				} else { // not the first van of the location, so include ","
					returnString += ", " + campervan.getName();
				}
				
			}
			
			if (numRequested[0] == 0 && numRequested[1] == 0) {
				break;
			}
			
		}
		if (firstOfLocation == false) {
			returnString += "; ";
		}
		return returnString;
	}
	
	/**
	 * Unassigns vans for a given booking ID, iterating through the campervans
	 *   and passing them a command to delete the associated booking
	 * @param ID the ID of the associated booking
	 */
	public void unassignVans(int ID) {
		for (Campervan campervan : campervans) {
			campervan.cancelBooking(ID);
		}
	}

	/**
	 * Goes through each campervan and requests a string from the van
	 *   of its bookings
	 * @return returnString a string containing all the vans and their bookings
	 */
	public String bookingPrinter() {
		String returnString = "";
		for (Campervan campervan : campervans) {
			returnString += campervan.printBookings(location);
		}
		return returnString;
	}

	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Depot other = (Depot) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}
}
