import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Acts as the backend for the campervan rental system
 * Takes the file input from VanRentalSystem, parses the commands, and puts them through to HeadOffice
 * @author Justin Ng
 *
 */
public class VanRentalSystemBackend {

	private HeadOffice headOffice = new HeadOffice();
	
	/**
	 * Constructor for the VanRentalSystem
	 */
	public VanRentalSystemBackend() {
	}
	
	/**
	 * Parses the commands, and passes the resulting information to the relevant
	 *   functions in headOffice
	 * Takes the responses from the headOffice functions and outputs them
	 * @param fileInput the reference to the file being input into the system
	 */
	public void runCommands(Scanner fileInput) {
		while (fileInput.hasNext()) {
			String input = fileInput.next();
			try {
				if (input.equals("Location")) {
					// Location <location> <name> <transmission>
					
					String location = fileInput.next();
					String name = fileInput.next();
					String transmission = fileInput.next();

                    headOffice.newVanForDepot(location, name, transmission);
                    
                } else if (input.equals("Request") || input.equals("Change")) {
                	// Request / Change <id> <startTime> <startMonth> <startDay>
                	//   <endTime> <endMonth> <endDay> <numT1> <t1> [<numT2> <t2>]
                	
                	// set up numbers to hold auto and manual requested
                	int auto = 0;
                	int manual = 0;
                	
                	// parse in the values for the booking
                	int ID = fileInput.nextInt();
                	
                	int startTime = fileInput.nextInt();
                	String startMonth = fileInput.next();
                	int startDay = fileInput.nextInt();
                	
                	int endTime = fileInput.nextInt();
                	String endMonth = fileInput.next();
                	int endDay = fileInput.nextInt();
                	
                	int numT1 = fileInput.nextInt();
                	String t1 = fileInput.next();
                	if (t1.equals("Automatic")) {
                		auto = numT1;
                	} else if (t1.equals("Manual")) {
                		manual = numT1;
                	}
                	
                	if (t1.equals("Automatic")) {
                		auto = numT1;
                	} else if (t1.equals("Manual")) {
                		manual = numT1;
                	}
                	
                	if (fileInput.hasNextInt()) {
                		int numT2 = fileInput.nextInt();
                    	String t2 = fileInput.next();
                    	
                    	if (t2.equals("Automatic")) {
                    		auto = numT2;
                    	} else if (t2.equals("Manual")) {
                    		manual = numT2;
                    	}
                	}
                	
                	// set up the TimeSlot to be input
                	TimeSlot reqSlot = new TimeSlot(startTime, startMonth, startDay, endTime, endMonth, endDay);
                	
                	// decide upon whether it was a request or a change, then output the response string
                	if (input.equals("Request")) {
                		System.out.println("Booking " + headOffice.newBooking(ID, auto, manual, reqSlot));
                	} else if (input.equals("Change")) {
                		System.out.println("Change " + headOffice.changeBooking(ID, auto, manual, reqSlot));
                	}
	                	
                } else if (input.equals("Cancel")) {
                	// Cancel <id>
                	int ID = fileInput.nextInt();
                	System.out.println("Cancel " + headOffice.cancelBooking(ID));
                } else if (input.equals("Print")) {
                	// Print <location>
                	String location = fileInput.next();
                	System.out.print(headOffice.locationBookingPrinter(location));
                } else {
                	fileInput.nextLine();
                }
			} catch (InputMismatchException e) {
				System.out.println("Inputted command " + input + " does not match any known commands");
			}
		}
	}
}
