/**
 * A class that holds the information for a booking, including the ID and the time slot
 * @author Justin Ng
 */
public class Booking implements Comparable<Booking>{
	
	private int ID;
	private TimeSlot timeSlot;
	
	/**
	 * Constructor for a Booking
	 * @param ID
	 * @param timeSlot
	 */
	public Booking(int ID, TimeSlot timeSlot) {
		this.ID = ID;
		this.timeSlot = timeSlot;
	}
	
	/**
	 * @return ID the ID of the booking
	 */
	public int getID() {
		return ID;
	}
	
	/**
	 * @return timeSlot the time slot of the booking
	 */
	public boolean bookingClash(TimeSlot reqSlot) {
		return timeSlot.hasTimeClash(reqSlot);
	}
	
	public int compareTo(Booking booking) {
		return timeSlot.compareTo(booking.timeSlot);
	}
	
	@Override
	public String toString() {
		return timeSlot.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Booking other = (Booking) obj;
		if (ID != other.ID)
			return false;
		return true;
	}
}
