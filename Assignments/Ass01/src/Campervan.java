import java.util.ArrayList;
import java.util.Collections;

/**
 * A class that holds the information for a campervan
 * Also interacts with the campervan's bookings, and checks for its availability
 * @author Justin Ng
 */
public class Campervan {
	
    private String name;
    private String transmission;
    private ArrayList<Booking> bookings = new ArrayList<Booking>();
	
    /**
     * Constructor for campervan
     * @param location     the location of the campervan
     * @param name         the name of the campervan
     * @param transmission the transmission of the campervan
     */
    public Campervan(String location, String name, String transmission) {
    	this.name = name;
        this.transmission = transmission;
    }
    
    /**
     * Takes the booking requested and adds it to the ArrayList<Booking> bookings
     * @param ID      the booking ID
     * @param reqSlot the requested time slot for the booking
     */
    public void setBooking(int ID, TimeSlot reqSlot) {
    	bookings.add(new Booking(ID, reqSlot));
    }
    
    /**
     * Takes the booking ID and removes the booking associated from ArrayList<Booking> bookings
     * @param ID the ID of the booking to be cancelled
     */
    public void cancelBooking(int ID) {
		for (Booking booking : bookings) {
			if (booking.getID() == ID) {
				bookings.remove(booking);
				break;
			}
		}
    }
    
    /**
     * Takes the requested time slot, and checks through the ArrayList<Booking> bookings
     *   to find any clashes
     * @param reqSlot  the requested time slot
     * @return boolean whether or not there is a clash
     */
    public boolean isAvailable(TimeSlot reqSlot) {
    	// the campervan is available in the requested time slot
    	if (bookings.isEmpty()) {
    		return true;
    	} else {
    		boolean hasClash = false;
    		for (Booking booking : bookings) {
    			if (booking.bookingClash(reqSlot)) {
    				hasClash = true;
    				break;
    			}
    		}
    		
    		if (hasClash == true) {
    			return false;
    		} else {
    			return true;
    		}
    	}
    }
    
    /**
     * Returns whether or not the van is available to be changed for a requested time slot and booking ID
     * @param ID       the ID of the booking changed
     * @param reqSlot  the time slot requested for the change
     * @return boolean whether or not the van is available for the change
     */
    public boolean isAvailableForChange(int ID, TimeSlot reqSlot) {
    	// the campervan is available in the requested time slot
    	if (bookings.isEmpty()) {
    		return true;
    	} else {
    		boolean hasClash = false;
    		for (Booking booking : bookings) {
    			if (booking.bookingClash(reqSlot) && ID != booking.getID()) {
    				hasClash = true;
    				break;
    			}
    		}
    		if (hasClash == true) {
    			return false;
    		} else {
    			return true;
    		}
    	}
    }
    
	/**
	 * Sorts the bookings for the campervan in time order, then returns a string of all
	 *   the vans bookings
	 * @param location      the location of the van
	 * @return returnString a string containing this van's bookings
	 */
	public String printBookings(String location) {
		Collections.sort(bookings);
		String returnString = "";
		for (Booking booking : bookings) {
			returnString = returnString + location + " " + name + " " + booking.toString() + "\n";
		}
		return returnString;
	}
    
    /**
     * @return name the name of the van
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return boolean whether or not the van is automatic
     */
    public boolean isAutomatic() {
    	return transmission.equals("Automatic");
    }
    
    /**
     * @return boolean whether or not the van is manual
     */
    public boolean isManual() {
    	return transmission.equals("Manual");
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campervan other = (Campervan) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
