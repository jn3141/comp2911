import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A class that represents a time slot, with calendar variables that hold the start and end of the slot
 * Also has functions to check for overlaps with other time slots
 * @author Justin Ng
 */
public class TimeSlot implements Comparable<TimeSlot>{
	
    private Calendar start = Calendar.getInstance();
    private Calendar end = Calendar.getInstance();
	
	/** Constructor for TimeSlot
	 * @param startTime  hour which the slot starts
	 * @param startMonth month which the slot starts
	 * @param startDay   day of the month which the slot starts
	 * @param endTime    hour which the slot ends
	 * @param endMonth   month which the slot ends
	 * @param endDay     day of the month which the slot ends
	 */
	public TimeSlot(int startTime, String startMonth, int startDay, int endTime, String endMonth, int endDay) {
		// Set start month from a parsed value
		try {
			Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(startMonth);
			this.start.setTime(date);
		} catch (ParseException e) {
			throw new IllegalArgumentException(startMonth + " is not a valid month.");
		}
		
		// Set start values
		// Set start time, day respectively
		this.start.set(Calendar.HOUR_OF_DAY, startTime);
		this.start.set(Calendar.DAY_OF_MONTH, startDay);
		
		// Set start year
		this.start.set(Calendar.YEAR, 2017);
		
		// Set end month from parsed value
		try {
			Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(endMonth);
			this.end.setTime(date);
		} catch (ParseException e) {
			throw new IllegalArgumentException(endMonth + " is not a valid month.");
		}
		
		// Set end values
		// Set end time, day respectively
		this.end.set(Calendar.HOUR_OF_DAY, endTime);
		this.end.set(Calendar.DAY_OF_MONTH, endDay);

		// Set end year
		this.end.set(Calendar.YEAR, 2017);
	}

	/**
	 * Compares the time slot with a requested time slot to check for overlap
	 * Sees if the start of 1 is before the end of 2, and the
	 *   start of 2 is before the end of 1
	 * Case 1                | Case 2
	 * 1 <--------->         |        1 <--------->
	 *        2 <--------->  | 2 <--------->
	 * @param reqSlot  the requested time slot
	 * @return boolean whether there was a time clash or not
	 */
	public boolean hasTimeClash(TimeSlot reqSlot) {
		Calendar realisticStart = (Calendar) start.clone();
		Calendar realisticEnd = (Calendar) end.clone();
		realisticStart.add(Calendar.HOUR_OF_DAY, -1);
		realisticEnd.add(Calendar.HOUR_OF_DAY, 1);
		return realisticStart.before(reqSlot.end) && reqSlot.start.before(realisticEnd);
	}
	
	public int compareTo(TimeSlot other) {
		Calendar otherStart = other.start;
		return start.compareTo(otherStart);
	}
	
	@Override
	public String toString() {
		SimpleDateFormat startSimple = new SimpleDateFormat("HH:00 MMM dd");
		String startString = startSimple.format(start.getTime());
		
		SimpleDateFormat endSimple = new SimpleDateFormat("HH:00 MMM dd");
		String endString = endSimple.format(end.getTime());
		
		return startString + " " + endString;
	}
	
}