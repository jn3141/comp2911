import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Runs the Freight System
 * It takes the file, parses the commands, and constructs the graph, followed by running jobs
 * @author Justin Ng, z5116690
 *
 * Complexity of the heuristic:
 *   Time complexity:
 *   	The heuristic is dependent on the use of the Floyd-Warshall algorithm, which has
 *      2*n^3 total operations, where n is |V| of the graph, giving the algorithm an O(n^3)
 *      time complexity.
 *      
 *      The resulting map of shortest distances from Floyd-Warshall has constant time for access,
 *      as it is a HashMap (i.e. O(1) complexity)
 *      
 *      Letting m be |jobs remaining|, because the heuristic adds the gaps between each job to
 *      an array list, the number of gaps that need to be looked up from Floyd-Warshall is m^2,
 *      and adding each element has O(1) complexity, so this job lookup/adding has O(m^2) complexity.
 *      
 *      On sorting this list, Collections.sort() has O(m*log(m)) complexity.
 *      
 *      Retrieving the m-1 lowest jobs, it has O(m) complexity.
 *      
 *      Thus, letting T(n) be the total number of operations made throughout one run of the heuristic:
 *      	T(n) = m^2 + m*log(m)) + (m-1)
 *      This is alongside a single 2*n^3 number of operations for Floyd-Warshall
 *   	
 *   	
 *   Space complexity:
 *   	The heuristic is dependent on the use of the Floyd-Warshall algorithm, which has
 *      2 n*n arrays, where n is |V| of the graph, giving the algorithm an O(n^2) complexity.
 *      
 *      Letting m be |jobs remaining|, because the heuristic adds the gaps between each job to
 *      an array list, the number of gaps that need to added to it is m^2, giving this an O(m^2)
 *      complexity.
 *      
 *      Thus, letting Z(n) be the amount of memory taken up by one run of the heuristic:
 *      	Z(n) = m^2
 *      This is alongside a single 2*n^2 amount of memory for Floyd-Warshall
 *      
 *      Of note is that the heuristic finding gaps between jobs generally keeps the m value low.
 *   
 *   Obviously enough:
 *     Best case: low job count
 *     Worst case: high job count
 */
public class FreightSystem {
	
	private static Graph freightMap = new Graph();
	private static AStarSearch aStarSearch = new AStarSearch();
	
	/**
	 * Takes the file, opens it, and passes it onto the command parser / 'runner'
	 * Then the Floyd-Warshall algorithm is run, followed by the A* Search
	 * Once all of this is done, it closes the file
	 * @param args contains the arguments of the command line input; args[0] contains the file itself.
	 */
	public static void main(String[] args) {
		Scanner freightInput = null;
		try {
			freightInput = new Scanner(new FileReader(args[0]));
			parseCommands(freightInput);
			freightMap.generateFloyd();
			System.out.print(aStarSearch.buildJobList(freightMap));
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} finally {
			if (freightInput != null) {
				freightInput.close();
			}
		}
	}
	
	/**
	 * Parses the commands, and passes the resulting information to the relevant
	 *   functions
	 * Takes the responses and outputs them
	 * @param freightInput    the reference to the file being input into the system
	 * @pre  freightMap does not contain any nodes / edges, and aStarSearch does not contain
	 *         any jobs
	 * @post freightMap contain all the nodes / edges, and aStarSearch does contains all the
	 *         jobs
	 */
	public static void parseCommands(Scanner freightInput) {
		while (freightInput.hasNext()) {
			String input = freightInput.next();
			try {
				if (input.equals("Unloading")) {
					// Unloading <cost> <town>
					
					int unloadCost = freightInput.nextInt();
					String town = freightInput.next();
					
					freightMap.addNode(town, unloadCost);
                    
                } else if (input.equals("Cost")) {
                	// Cost <cost> <town1> <town2>
                	
					int distanceBetween = freightInput.nextInt();
					String town1 = freightInput.next();
					String town2 = freightInput.next();
                	
					freightMap.addEdge(distanceBetween, town1, town2);
					
                } else if (input.equals("Job")) {
                	// Job <town1> <town2>
                	
					String town1 = freightInput.next();
					String town2 = freightInput.next();
					
					aStarSearch.addJob(town1, town2, freightMap);
                } else {
                	freightInput.nextLine();
                }
			} catch (InputMismatchException e) {
				System.out.println("Inputted command " + input + " does not match any known commands");
			}
		}
	}
}
