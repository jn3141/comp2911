import java.util.ArrayList;
import java.util.Collections;

/**
 * Implements a heuristic for the Heuristics interface
 * @author Justin Ng, z5116690
 *
 */
public class HeuristicA implements Heuristics {
	
	/*
	 * It utilises the distanceArray of the map, which holds the shortest distances between every node
	 * The heuristic value is essentially the n-1 number of lowest cost possible pursuits added
	 *   together, with this n value being the number of jobs
	 * Use n-1 as if there are n jobs, there are n-1 gaps, so this assumes the best case that the
	 *   n-1 lowest cost movements between jobs are enough to finish the n remaining jobs
	 *   E.g. have jobs A, B, C remaining with the following costs to go from the job on the left
	 *        to the jobs on the right:
	 *        A: B-100 C-200
	 *        B: A-300 C-400
	 *        C: A-500 B-600
	 *        it would take the 2 lowest costs as there are 3 jobs remaining:
	 *        heuristic = 100 + 200
	 * @param map                     the graph representation of the map, and contains the 2D array
	 *                                  with the shortest distance between each and every node
	 * @param shortestDistanceBetween used to get the shortest distance between two nodes
	 * @param allDistances            holds all the costs between jobs, and is sorted afterwards
	 */
	public static int calculateHeuristic(Graph map, ArrayList<JobVector> jobsRemaining) {
		int heuristic = 0;
		int shortestDistanceBetween;
		ArrayList<Integer> allDistances = new ArrayList<Integer>();
		
		// add all of the distances between jobs to an ArrayList<Integer>, then sort it
		for (JobVector outerRemaining : jobsRemaining) {
			String outerEnd = outerRemaining.getEnd();
			for (JobVector innerRemaining : jobsRemaining) {
				String innerStart = innerRemaining.getStart();
				shortestDistanceBetween = map.getShortestDistanceBetween(outerEnd, innerStart);
				allDistances.add(shortestDistanceBetween);
			}
		}
		
		Collections.sort(allDistances);
		
		// take the front n-1 gap costs, and add them to the heuristic
		for (int i = 0; i < (jobsRemaining.size()-1); i++){
			heuristic += allDistances.get(i);
		}
		
		return heuristic;
	}
	
}
