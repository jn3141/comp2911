import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * The class the controls the actual searching for paths
 * It holds all the jobs, and the weights for unloading and moving during the actual jobs themselves
 * This method abstracts the path finding further than just building node lists until the jobs are all
 *   within the node list
 * It does this by searching through and checking for the gaps between the jobs; essentially, it builds a path
 *   of jobs, and creates a job order
 * After building this job order, it then takes the resulting list of jobs and builds it into the full set of steps
 *    by going through the jobs and building the gaps that were pre-generated as part of the Floyd-Warshall algorithm
 * @author Justin Ng, z5116690
 *
 */
public class AStarSearch {
	private ArrayList<JobVector> jobs;
	private int totalWeight;
	private int totalUnload;
	
	/**
	 * Creates a new AStarSearch instance
	 * @param jobs        an ArrayList<TravelVector> that holds the jobs
	 * @param totalWeight the total weight of traveling across jobs ONLY
	 * @param totalUnload the total cost of unloading for the jobs
	 */
	public AStarSearch() {
		jobs = new ArrayList<JobVector>();
		totalWeight = 0;
		totalUnload = 0;
	}
	
	/**
	 * Adds a new job to the ArrayList<TravelVector> jobs
	 * @param  job             the TravelVector of the job
	 * @param  distanceBetween the distance between the two nodes of a job
	 * @param  unloadCost      the cost to unload at the end of a job
	 * @return boolean         whether or not the job was added successfully
	 * @pre  jobs does not contain job, where job is between town1 and town2
	 * @post jobs contains job, where job is between town1 and town2
	 */
	public void addJob(String town1, String town2, Graph map) {
		JobVector job = new JobVector(town1, town2);
		jobs.add(job);
		totalWeight += map.getDistanceBetween(town1, town2);
		totalUnload += map.getUnloadCost(town2);
	}
	
	/**
	 * The actual search function itself
	 * It works by checking for the gaps between the jobs and building those up
	 * After building the gaps, then it takes the resulting Path and builds it into the full set of steps
	 *    including the gaps and the jobs themselves
	 * @param  map            the Graph holding the whole map of nodes and related data
	 * @param  expanded       the number of expanded nodes off the PQ
	 * @param  pathComparator comparator used to compare Paths held in PQ
	 * @param  priorityQ      the priority queue itself
	 * @param  current        Path object that starts from Sydney, or has just been popped out of the PQ
	 * @param  next           Path object that has new possibility appended to the current just popped out of the PQ
	 * @param  jobsRemaining  used to store the remaining jobs of a popped off PQ Path
	 * @param  returnString   string that is part of the resulting returned String
	 * @return String         a string holding the number of expanded nodes, the cost of the whole trip, and the steps taken
	 * @pre  do not have path and cost for the freight trip, nor the number of nodes expanded
	 * @post have a path and the cost for the freight, as well as the number of nodes expanded
	 */
	public String buildJobList(Graph map) {
		// check all jobs for connections between them
		// if they can't all be connected, then return no solution
		for (JobVector outerRemaining : jobs) {
			String outerEnd = outerRemaining.getEnd();
			for (JobVector innerRemaining : jobs) {
				String innerStart = innerRemaining.getStart();
				if (map.getShortestDistanceBetween(outerEnd, innerStart) == -1) {
					return ("No solution\n");
				}
			}
		}
		
		int expanded = 0;
		
		PriorityQueue<Path> priorityQ = new PriorityQueue<Path>(100);

		Path current = null;
		Path next = null;
		
		ArrayList<JobVector> jobsRemaining = null;
		
		String returnString = "";
		
		// add all the jobs that start with Sydney to the PQ
		for (JobVector job : jobs) {
			if (!job.getStart().equals("Sydney")) {
				continue;
			}
			
			current = new Path();
			current.setJobsRemaining(jobs);
			current.tickOffJob(job);
			current.defineHeuristic(map);
			priorityQ.add(current);
		}
		
		// start popping off Paths from the PQ and checking them for goal states
		while (!priorityQ.isEmpty()) {
			current = priorityQ.poll();
			expanded++;
			
			// if current has finished its jobs, break as it has reached goal state found
			if (current.finishedJobs()) {
				break;
			}
			
			// haven't reached goal state
			// append to the current jobList each of the remaining jobs and add to the PQ
			String endOfLastJob = current.getLastJobLocation();
			jobsRemaining = current.getJobsRemaining();
			for (JobVector remainingJob : jobsRemaining) {
				String startOfRemainingJob = remainingJob.getStart();
				int distanceBetween = map.getShortestDistanceBetween(endOfLastJob, startOfRemainingJob);
				next = current.clone();
				next.addToWeight(distanceBetween);
				next.tickOffJob(remainingJob);
				next.defineHeuristic(map);
				priorityQ.add(next);
			}
		}
		
		// found the goal state with the jobs in order, so take the Path and reconstruct it into a String
		returnString = current.pathToString(map);
		return (expanded + " nodes expanded\ncost = " + (totalWeight + totalUnload + current.getTotalWeight()) + "\n" + returnString);
	}
}
