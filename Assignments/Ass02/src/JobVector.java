
/**
 * JobVector class holds the start and end of a job between two adjacent nodes
 * @author Justin Ng, z5116690
 *
 */
public class JobVector implements Cloneable {
	String start;
	String end;
	
	/**
	 * Creates a new JobVector
	 * @param start String of location this vector started at
	 * @param end   String of location this vector ended at
	 */
	public JobVector(String start, String end) {
		this.start = start;
		this.end = end;
	}

	/**
	 * @return start String of location this vector started at
	 */
	public String getStart() {
		return start;
	}
	
	/**
	 * @return end String of location this vector ended at
	 */
	public String getEnd() {
		return end;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobVector other = (JobVector) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return ("Job " + start + " to " + end + "\n");
	}

	@Override
	public JobVector clone() {
		try {
			return (JobVector) super.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("Cloning not allowed in TravelVector.\n");
			return this;
		}
	}
}
