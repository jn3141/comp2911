import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;


/**
 * A representation of a graph using an adjacency matrix represented with HashMaps,
 *   and only includes values between nodes that actually have edges
 * Alongside this, there is a HashMap with unloadCosts, as well as various
 *   HashMaps utilised in Floyd-Warshall algorithms
 * For more info on the Floyd-Warshall algorithm, please see the following:
 *   https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm
 * @author Justin Ng, z5116690
 *
 */
public class Graph {
	// basic map representation
	private HashMap<String, HashMap<String, Integer>> nodeMap;
	private HashMap<String, Integer> unloadHashMap;
	private int numNodes;
	
	// for Floyd-Warshall
	private HashMap<String, HashMap<String, Integer>> distanceArray;
	private HashMap<String, HashMap<String, String>> routes;

	/**
	 * @param nodeMap        adjacency matrix representation of a graph using HashMaps; only includes values
	 *                         between nodes that actually have edges
	 * @param unloadHashMap  HashMap that provides quick lookup for unload costs for each town
	 * @param distanceArray  shortest distance between each and every node; includes
	 *                         0 between a node and itself, and a -1 / infinity for a disconnected node
	 * @param routes         path between each node
	 */
	public Graph() {
		nodeMap = new HashMap<String, HashMap<String, Integer>>();
		unloadHashMap = new HashMap<String, Integer>();
		numNodes = 0;
		
		distanceArray = new HashMap<String, HashMap<String, Integer>>();
		routes = new HashMap<String, HashMap<String, String>>();
	}

	/**
	 * Adds a node to the graph
	 * @param name         name of the location
	 * @param unloadCost   cost of unloading at the location
	 * @pre  nodeMap does not have the node with the town, nor does unloadHashMap have 
	 *         the node with the town and unload cost
	 * @post nodeMap contains the node with the town, unloadHashMap contains the
	 *          node with the town and unload cost
	 */
	public void addNode(String town, int unloadCost) {
		// create a new node and it's associated adjacency row/column
		HashMap<String, Integer> newMapNode = new HashMap<String, Integer>();
		
		// add the node and its unloadCost to the map, and increment the numNodes
		nodeMap.put(town, newMapNode);
		numNodes++;
		unloadHashMap.put(town, unloadCost);
		
		// create new nodes and their associated adjacency row/column for Floyd-Warshall
		// also make the distances to itself 0
		HashMap<String, Integer> newDNode = new HashMap<String, Integer>();
		HashMap<String, String> newRNode = new HashMap<String, String>();
		
		// add the nodes to their Floyd-Warshall HashMaps
		distanceArray.put(town, newDNode);
		routes.put(town, newRNode);
	}

	/**
	 * Adds a edge between two nodes in the graph
	 * @param distanceBetween distance between the two towns
	 * @param town1           name of first town
	 * @param town2           name of second town
	 * @pre  edge does not exist between town1 and town2 w/ weight
	 * @post edge exists etween town1 and town2 w/ weight
	 */
	public void addEdge(int distanceBetween, String town1, String town2) {
		// get the adjacency rows/columns for each town
		HashMap<String, Integer> node1 = nodeMap.get(town1);
		HashMap<String, Integer> node2 = nodeMap.get(town2);
		
		// add the towns to each other's adjacency rows/columns
		node1.put(town2, distanceBetween);
		node2.put(town1, distanceBetween);
	}

	/**
	 * Generates the shortest distances, and the routes between, each node using the Floyd-Warshall algorithm
	 * @param (outer/inner/core)It       iterators for outer/inner/core loops
	 * @param (outer/inner/core)Pair     the pairs of keys/values for outer/inner/core loops
	 * @param (outer/inner/core)NodeName the keys/names of the towns for the iterations through the
	 *                                     outer/inner/core loops
	 * @param distanceArrayXY            used as part of the Floyd-Warshall algorithm, they hold the 'array'
	 *                                     distances being worked on
	 * @param routesIK                   used as part of the Floyd-Warshall algorithm, it holds the 'route'
	 *                                     town reference
	 * @pre  distanceArray and routes is empty
	 * @post distanceArray is filled with shortest paths between nodes, and the corresponding routes are filled
	 */
	public void generateFloyd() {
		// initialise variables
		Iterator<Entry<String, HashMap<String, Integer>>> outerIt;
		HashMap.Entry<String, HashMap<String, Integer>> outerPair;
		String outerNodeName;
		
		Iterator<Entry<String, HashMap<String, Integer>>> innerIt;
		HashMap.Entry<String, HashMap<String, Integer>> innerPair;
		String innerNodeName;
		
		Iterator<Entry<String, HashMap<String, Integer>>> coreIt;
		HashMap.Entry<String, HashMap<String, Integer>> corePair;
		String coreNodeName;

		HashMap<String, Integer> distanceArrayEntry;
		int distanceArrayIK;
		int distanceArrayKJ;
		int distanceArrayIJ;
		
		String routesIK;
		
		// Duplicate nodeMap into distanceArray, by iterating through the nodes with the additional:
		//   -1s / infinity filling no connections
		//   0s filling spaces between a node and itself in the array
		// Set up routes
		outerIt = nodeMap.entrySet().iterator();
	    while (outerIt.hasNext()) {
	    	outerPair = (HashMap.Entry<String, HashMap<String, Integer>>) outerIt.next();
	    	outerNodeName = outerPair.getKey();
	        innerIt = nodeMap.entrySet().iterator();
	        while (innerIt.hasNext()) {
	        	innerPair = (HashMap.Entry<String, HashMap<String, Integer>>) innerIt.next();
	        	innerNodeName = innerPair.getKey();
	        	
	        	distanceArrayEntry = distanceArray.get(outerNodeName);
	        	// if it's the same place, then put a 0;
	        	//   else if there's no connection between the two nodes, add a -1 / infinity;
	        	//   else just add the distance between them
	        	if (outerNodeName.equals(innerNodeName)) {
	        		distanceArrayEntry.put(innerNodeName, 0);
	        	} else if (outerPair.getValue().get(innerNodeName) == null) {
	        		distanceArrayEntry.put(innerNodeName, -1);
	        	} else {
	        		distanceArrayEntry.put(innerNodeName, outerPair.getValue().get(innerNodeName));
	        		routes.get(outerNodeName).put(innerNodeName, innerNodeName);
	        	}
			}
	    }
	    
	    // Construct the shortest distances between each node, and the paths between them
	    //   using Floyd-Warshall
		outerIt = distanceArray.entrySet().iterator();
		for (int k = 0; k < numNodes; k++) {
			outerPair = (HashMap.Entry<String, HashMap<String, Integer>>) outerIt.next();
			outerNodeName = outerPair.getKey();
			innerIt = distanceArray.entrySet().iterator();
			for (int i = 0; i < numNodes; i++) {
				innerPair = (HashMap.Entry<String, HashMap<String, Integer>>) innerIt.next();
				innerNodeName = innerPair.getKey();
				coreIt = distanceArray.entrySet().iterator();
				for (int j = 0; j < numNodes; j++) {
					corePair = (HashMap.Entry<String, HashMap<String, Integer>>) coreIt.next();
					coreNodeName = corePair.getKey();
					distanceArrayIK = distanceArray.get(innerNodeName).get(outerNodeName);
					distanceArrayKJ = distanceArray.get(outerNodeName).get(coreNodeName);
					distanceArrayIJ = distanceArray.get(innerNodeName).get(coreNodeName);
					
					// make sure we aren't adding together -1 / infinity
					if (distanceArrayIK != -1 && distanceArrayKJ != -1) {
						// if (distanceArray[i][j] > distanceArray[i][k] + distanceArray[k][j]), then
						//     set distanceArray[i][j] = distanceArray[i][k] + distanceArray[k][j]
						if (distanceArrayIJ > distanceArrayIK + distanceArrayKJ || (distanceArrayIJ == -1)) {
							distanceArray.get(innerNodeName).put(coreNodeName, (distanceArrayIK + distanceArrayKJ));
							routesIK = routes.get(innerNodeName).get(outerNodeName);
							routes.get(innerNodeName).put(coreNodeName, routesIK);
						}
					}
				}
			}
		}
	}

	/**
	 * Constructs a string of the steps taken to get from one node to another using routes
	 * @param  jobEnd       the end of the prior job
	 * @param  jobStart     the start of the next job
	 * @return returnString the string containing all the steps taken to move between the jobs
	 * @pre  string with the route between nodes does not exist
	 * @post string with the route between nodes exists
	 */
	public String stepsBetween(String jobEnd, String jobStart) {
		String startLocation = jobEnd;
		String endLocation = jobStart;
		String returnString = "";
		
		while (!startLocation.equals(endLocation)) {
			returnString = returnString + "Empty " + startLocation + " to ";
			startLocation = routes.get(startLocation).get(endLocation);
			returnString = returnString + startLocation + "\n";
		}
		return returnString;
	}
	
	/**
	 * Gets the shortest distance between two towns; doesn't have to be directly connected
	 * @param  town1 the first town to have distance looked up
	 * @param  town2 the second town to have distance looked up
	 * @return int   shortest distance between the two towns
	 */
	public int getShortestDistanceBetween(String town1, String town2) {
		return distanceArray.get(town1).get(town2);
	}
	
	/**
	 * Gets the direct distance between two towns connected with one edge
	 * @param  town1 the first town to have distance looked up
	 * @param  town2 the second town to have distance looked up
	 * @return int   direct distance between the two towns
	 */
	public int getDistanceBetween(String town1, String town2) {
		return nodeMap.get(town1).get(town2);
	}
	
	/**
	 * @param town
	 * @return
	 */
	public int getUnloadCost(String town) {
		return unloadHashMap.get(town);
	}
}
