import java.util.ArrayList;

/**
 * An Interface for Heuristics
 * @author Justin Ng, z5116690
 *
 */
public interface Heuristics {
	
	/**
	 * Defines the heuristic for a Path object
	 * @param map           the graph representation of the map, and contains the 2D array
	 *                        with the shortest distance between each and every node
	 * @param jobsRemaining ArrayList of remaining jobs
	 */
	public static int calculateHeuristic(Graph map, ArrayList<JobVector> jobsRemaining) {
		return 0;
	}
}
