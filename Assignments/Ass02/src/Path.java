import java.util.ArrayList;

/**
 * An object that is used as part of the PQ in A* Search
 * It holds the order in which jobs are done, the remaining jobs,
 *   as well as the total weight and heuristic of running these
 *   steps
 * @author Justin Ng, z5116690
 *
 */
public class Path implements Comparable<Path>{
	private ArrayList<JobVector> jobsList;
	private ArrayList<JobVector> jobsRemaining;
	private int totalWeight;
	private int heuristic;

	/**
	 * Creates a new Path object
	 * @param jobList       a list of jobs in the order they're completed
	 * @param jobsRemaining a list of jobs yet to be completed
	 * @param totalWeight   the total weight of moving between jobs
	 * @param HeuristicA     the heuristic for going to down this path.\
	 */
	public Path() {
		jobsList = new ArrayList<JobVector>();
		jobsRemaining = new ArrayList<JobVector>();
		totalWeight = 0;
		heuristic = 0;
	}
	
	/**
	 * Adds to the total weight the cost it would take to move from previous job
	 *   to the proposed job
	 * @param distanceBetween distance between the previous job and the job to be
	 *                          completed
	 * @pre  totalWeight does not include distanceBetween
	 * @post totalWeight includes the distanceBetween
	 */
	public void addToWeight(int distanceBetween) {
		totalWeight += distanceBetween;
	}
	
	/**
	 * Removes a completed job from the jobsRemainingg list, and adds it to the completed
	 *   jobList
	 * @param completedJob the job that was completed
	 * @pre  jobsRemaining contains the completedJob, whilst jobsList does not
	 * @post jobsRemaining does not contain the completedJob, whilst jobsList does
	 */
	public void tickOffJob(JobVector completedJob) {
		jobsRemaining.remove(completedJob);
		jobsList.add(completedJob);
	}
	
	/**
	 * Defines the heuristic for this Path object
	 *
	 * @param map          the graph representation of the map, and contains the 2D array
	 *                        with the shortest distance between each and every node
	 * @param lowestCost   used to keep track of the lowest cost next job for this
	 *                        remaining job; it's value is -1 as an 'infinity' value
	 * @param allDistances holds all the costs between jobs, and is sorted afterwards
	 * @pre  the heuristic is not defined
	 * @post the heuristic is defined
	 */
	public void defineHeuristic(Graph map) {
		heuristic = 0;
		heuristic = HeuristicA.calculateHeuristic(map, jobsRemaining);
	}
	
	/**
	 * Takes the jobList of completed jobs, and using their order, construct
	 *   a string that describes the list of steps taken to complete the jobs
	 * Further uses the slightly modified Floyd-Warshall algorithm to build the path
	 * @param  map            the graph representation of the map
	 * @param  firstJob       the previous job
	 * @param  firstJobEnd    the end of the previous job
	 * @param  secondJob      the next job
	 * @param  secondJobStart the start of the next job
	 * @return returnString   String containing all the steps taken, including jobs and gaps between
	 *                        the jobs
	 * @pre  string with the route between nodes does not exist
	 * @post string with the route between nodes exists
	 */
	public String pathToString(Graph map) {
		// initialise variables
		String returnString = "";
		
		JobVector firstJob;
		JobVector secondJob;
		
		returnString += jobsList.get(0).toString();
		
		// iterate through the jobs, one pair at a time, and get a constructed path
		//   of the steps between the jobs
		for (int i = 0; i < (jobsList.size()-1); i++){
		    firstJob = jobsList.get(i);
			String firstJobEnd = firstJob.getEnd();
			secondJob = jobsList.get(i+1);
		    String secondJobStart = secondJob.getStart();
			returnString += map.stepsBetween(firstJobEnd, secondJobStart);
			returnString += secondJob.toString();
		}
		
		return returnString;
	}
	
	/**
	 * @return boolean whether or not all the jobs are finished
	 */
	public boolean finishedJobs() {
		return jobsRemaining.isEmpty();
	}
	
	/**
	 * Used to order the Path objects for the PQ; this function is used by the PathComparator
	 * @return int f(n) = g(n) + h(n)
	 */
	public int ordering() {
		return totalWeight + heuristic;
	}
	
	/**
	 * @return String the location where the last job ended at
	 */
	public String getLastJobLocation() {
		JobVector lastVector = jobsList.get(jobsList.size() - 1);
		return lastVector.getEnd();
	}
	
	/**
	 * @return totalWeight the total weight of traveling between jobs
	 */
	public int getTotalWeight() {
		return totalWeight;
	}
	
	/**
	 * @return jobsRemaining the ArrayList<TravelVector> of remaining jobs
	 */
	public ArrayList<JobVector> getJobsRemaining() {
		return jobsRemaining;
	}
	
	/**
	 * @return heuristic the heuristic of this Path
	 */
	public int getHeuristic() {
		return heuristic;
	}
	
	/**
	 * @param jobs list of jobs to be completed
	 */
	public void setJobsRemaining(ArrayList<JobVector> jobs) {
		for (JobVector vector : jobs) {
		    jobsRemaining.add(vector.clone());
		}
	}

	@Override
    public int compareTo(Path y) {
        if (this.ordering() < y.ordering()) {
            return -1;
        }
        
        if (this.ordering() > y.ordering()) {
            return 1;
        }
        
        return 0;
    }
	
	/**
	 * doesn't use Cloneable as ArrayLists don't deep clone
	 */
	@Override
	public Path clone() {
		Path clone = new Path();
		for (JobVector step : this.jobsList) {
		    clone.jobsList.add(step.clone());
		}
		for (JobVector step : this.jobsRemaining) {
		    clone.jobsRemaining.add(step.clone());
		}
		clone.totalWeight = this.totalWeight;
		clone.heuristic = this.heuristic;
		return clone;
	}
}
