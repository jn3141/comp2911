# Tests disconnected jobs giving a 'No solution' result
# Min cost: No solution
Unloading 0 Sydney
Unloading 0 A
Unloading 0 B
Unloading 0 C
Unloading 0 D
Unloading 0 E
Unloading 0 F
Unloading 0 G
Unloading 0 H
Unloading 0 I
Unloading 0 J

Cost 30 Sydney B
Cost 70 B C
Cost 90 C D
Cost 20 D E
Cost 250 E F
Cost 20 F G
Cost 0 G Sydney

Cost 90 H I
Cost 10 I J

Job Sydney B
Job B C
Job C D
Job D E
Job E F
Job F G
Job G Sydney

Job H I
Job I J