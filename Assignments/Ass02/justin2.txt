# Tests straight line jobs that all point towards Sydney
# Min cost = 700
Unloading 0 Sydney
Unloading 0 A
Unloading 0 B
Unloading 0 C
Unloading 0 D
Unloading 0 E
Unloading 0 F
Unloading 0 G
Unloading 0 H

Cost 20 Sydney A
Cost 70 A B
Cost 30 B C
Cost 50 C D
Cost 10 D E
Cost 90 E F
Cost 30 F G
Cost 60 G H

Job Sydney A
Job B A
Job C B
Job D C
Job E D
Job F E
Job G F
Job H G