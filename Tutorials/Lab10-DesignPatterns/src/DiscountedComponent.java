
public class DiscountedComponent extends ComponentDecorator {

	private int percentageDiscount;
	
	public DiscountedComponent(Component component, int percentageDiscount) {
		super(component);
		this.percentageDiscount = percentageDiscount;
	}
	
	@Override
	public int calculatePrice() {
		int originalPrice = super.calculatePrice();
		return (int) (originalPrice - (originalPrice * ( (float) percentageDiscount / 100)));
	}
	
	@Override
	public String toString() {
		return this.getComponent().toString() + "\nBut with discount of " + percentageDiscount +
				"%\nNew price is now: " + calculatePrice();
	}
}
