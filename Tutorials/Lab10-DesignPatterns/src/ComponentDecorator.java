
public class ComponentDecorator implements Component{

	private Component component;
	
	public ComponentDecorator(Component component) {
		this.component = component;
	}
	
	public Component getComponent() {
		return component;
	}
	
	@Override
	public int calculatePrice() {
		return component.calculatePrice();
	}

}
