import java.util.ArrayList;

public class Assembly implements Component {
	private String assemblyName;
	private ArrayList<Component> itemList;
	
	public Assembly(String assemblyName) {
		this.assemblyName = assemblyName;
		itemList = new ArrayList<Component>();
	}
	
	public void addComponent(Component item) {
		itemList.add(item);
	}
	
	public void removeComponent(Component item) {
		itemList.remove(item);
	}
	
	public int calculatePrice() {
		int price = 0;
		for (Component item : itemList) {
			price += item.calculatePrice();
		}
		return price;
	}
	
	@Override
	public String toString() {
		/*String componentString = "";
		
		for (Component item : itemList) {
			componentString = componentString + item.toString() + "\n";
		}*/
		
		return "Assembly: " + assemblyName + "\nRRP: " + calculatePrice() /*+ "\nComponent list:\n" + componentString*/;
	}
}
