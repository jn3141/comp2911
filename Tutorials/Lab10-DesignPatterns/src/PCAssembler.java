
public class PCAssembler {
	public static void main (String args[]) {
		Assembly PC = new Assembly("Ryzen");
		
		Item CPU = new Item("i5", 200);
		System.out.println(CPU.toString());
		
		Item GPU = new Item("Nvidia", 600);
		System.out.println(GPU.toString());
		
		Item RAM = new Item("16GB", 100);
		System.out.println(RAM.toString());
		
		Item AIO = new Item("Cooler", 200);
		System.out.println(AIO.toString());
		
		DiscountedComponent cheapAIO = new DiscountedComponent(AIO, 10);
		System.out.println(cheapAIO.toString());
		
		PC.addComponent(CPU);
		PC.addComponent(GPU);
		PC.addComponent(RAM);
		PC.addComponent(cheapAIO);
		
		System.out.println(PC.toString());
		
		DiscountedComponent cheapPC = new DiscountedComponent(PC, 20);
		
		System.out.println(cheapPC.toString());
	}
}
