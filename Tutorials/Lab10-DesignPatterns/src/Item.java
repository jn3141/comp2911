
public class Item implements Component {
	private String itemName;
	private int price;
	
	public Item(String itemName, int price) {
		this.itemName = itemName;
		this.price = price;
	}
	
	public int calculatePrice() {
		return (short) price;
	}
	
	@Override
	public String toString() {
		return "Item: " + itemName + "\nPrice: " + price;
	}
}
