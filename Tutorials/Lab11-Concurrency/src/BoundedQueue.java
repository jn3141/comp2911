import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A first-in, first-out bounded collection of objects.
 */
public class BoundedQueue<E> {

	private Object[] elements;
	private int head;
	private int tail;
	private int size;
	private final ReentrantLock boundedQueueLock;
	private final Condition notFull;
	private final Condition notEmpty;

	/**
	 * Constructs an empty queue.
	 * 
	 * @param capacity
	 *            the maximum capacity of the queue
	 */
	public BoundedQueue(int capacity) {
		elements = new Object[capacity];
		head = 0;
		tail = 0;
		size = 0;
		boundedQueueLock = new ReentrantLock();
		notFull = boundedQueueLock.newCondition();
		notEmpty = boundedQueueLock.newCondition();
	}

	/**
	 * Removes the object at the head.
	 * 
	 * @return the object that has been removed from the queue
	 * @precondition !isEmpty()
	 */
	public E remove() {
		boundedQueueLock.lock();
		E r = null;
		try {
			while (isEmpty()) {
				notEmpty.await();
			}
			
			r = (E) elements[head];
			head++;
			size--;
	
			if (head == elements.length) {
				head = 0;
			}
			
			notFull.signalAll();
		} finally {
			boundedQueueLock.unlock();
			return r;
		}
	}

	/**
	 * Appends an object at the tail.
	 * 
	 * @param newValue
	 *            the object to be appended
	 * @return
	 * @precondition !isFull();
	 */
	public void add(E newValue) {
		boundedQueueLock.lock();
		
		try {
			while (isFull()) {
				notFull.await();
			}
			
			elements[tail] = newValue;
	
			tail++;
	
			size++;
	
			if (tail == elements.length) {
				tail = 0;
			}
			
			notEmpty.signalAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			boundedQueueLock.unlock();
		}
	}

	public boolean isFull() {
		return size == elements.length;
	}

	public boolean isEmpty() {
		return size == 0;
	}
}
