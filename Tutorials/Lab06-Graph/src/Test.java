
public class Test {
	
	public static void main(String args[]) {
		HashGraph<String> graph = new HashGraph<String>();
		
		graph.addNode("A");
		graph.addNode("B");
		graph.addNode("C");
		graph.addEdge("A", "B");
		graph.addEdge("A", "C");
		graph.addEdge("B", "C");
		graph.removeNode("A");
		
		System.out.println(graph.toString());
	}
	
}
