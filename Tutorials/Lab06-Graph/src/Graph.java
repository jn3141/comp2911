
public interface Graph<E> {
	
	public void addNode(E e);
	
	public void removeNode(E e);
	
	public void addEdge(E e1, E e2);
}
