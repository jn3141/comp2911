import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class HashGraph<E> implements Graph<E> {

	private HashMap<E, HashMap<E, Integer>> AdjacencyMatrix;
	private int numNodes = 0;
	
	public HashGraph() {
		AdjacencyMatrix = new HashMap<E, HashMap<E, Integer>>();
	}
	
	@Override
	public void addNode(E e) {
		// create a new node and it's associated adjacency row/column
		HashMap<E, Integer> newNode = new HashMap<E, Integer>();
		
		// add the node to the graph, and increment the numNodes
		AdjacencyMatrix.put(e, newNode);
		numNodes++;
	}

	@Override
	public void removeNode(E e) {
		Set<Entry<E, Integer>> eAdjacencySet = AdjacencyMatrix.get(e).entrySet();
		for (Entry<E, Integer> entryE : eAdjacencySet) {
			AdjacencyMatrix.get(entryE.getKey()).remove(e);
		}
		AdjacencyMatrix.remove(e);
		numNodes--;
	}

	@Override
	public void addEdge(E e1, E e2) {
		AdjacencyMatrix.get(e1).put(e2, 1);
		AdjacencyMatrix.get(e2).put(e1, 1);
	}
	
	@Override
	public String toString() {
		return AdjacencyMatrix.toString();
	}
}
