import java.time.LocalDate;

/**
 * Simple subclass of employee that represents a manager
 * @author Justin
 * @version 1.0
 */

public class Manager extends Employee implements Cloneable {
	
	private LocalDate hireDate;
	
	/**
	 * this is the constructor for this class
	 * it extends the superclass employee
	 * @param name     the name of the manager
	 * @param salary   the salary of the manager
	 * @param hireDate the date the manager was hired
	 */
	
	public Manager(String name, int salary, LocalDate hireDate) {
		super(name, salary);
		this.hireDate = hireDate;
	}
	
	/**
	 * method to set a manager's hire date
	 * @param hireDate 
	 */
	
	public void setHireDate(LocalDate hireDate) {
		this.hireDate = hireDate;
	}
	
	/**
	 * method to return a manager's hire date
	 * @return Calendar
	 */
	
	public LocalDate getHireDate() {
		return hireDate;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Hiring Date: " + hireDate.toString() + "\n";
	}
	
	@Override
	public boolean equals(Object obj) {
		Manager other = (Manager) obj;
		return super.equals(obj) && (hireDate.equals(other.hireDate));
	}
	
	@Override
	public Manager clone() {
			Manager other = (Manager) super.clone();
			other.hireDate = this.hireDate;
			return other;
	}
}
