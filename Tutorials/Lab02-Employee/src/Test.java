import java.time.LocalDate;

public class Test {

	public static void main(String[] args) {
		
		System.out.println("---Testing out Employee generally---");
		Employee a = new Employee("Alan", 50000);
		System.out.println(a.toString());
		Employee b = new Employee("Brad", 100000);
		System.out.println(b.toString());
		
		System.out.println("---Testing out Manager generally---");
		LocalDate cDate = LocalDate.parse("2017-01-01");
		LocalDate dDate = LocalDate.parse("2012-02-29");
		Manager c = new Manager("Connor", 150000, cDate);
		System.out.println(c.toString());
		Manager d = new Manager("David", 200000, dDate);
		System.out.println(d.toString());
		
		System.out.println("---Testing out equals---");
		Employee aCopy = a.clone();
		Employee bCopy = b.clone();
		Manager  cCopy = c.clone();
		Manager  dCopy = d.clone();
		System.out.println("This is a clone of Alan:\n" + aCopy.toString() + "Are they identical? (T/F) " + a.equals(aCopy) + "\n");
		System.out.println("This is a clone of Brad:\n" + bCopy.toString() + "Are they identical? (T/F) " + b.equals(bCopy) + "\n");
		System.out.println("This is a clone of Connor:\n" + cCopy.toString() + "Are they identical? (T/F) " + c.equals(cCopy) + "\n");
		System.out.println("This is a clone of David:\n" + dCopy.toString() + "Are they identical? (T/F) " + d.equals(dCopy) + "\n");
		
		System.out.println("---Creating an Employee and Manager with the same values except for hireDate---");
		Employee eOne = new Employee("Edward", 50000);
		LocalDate eDate = LocalDate.parse("2010-10-10");
		Manager eTwo = new Manager("Edward", 50000,eDate);
		System.out.println("This is Edward the First:\n" + eOne.toString() + "\nThis is Edward the Second:\n" + eTwo.toString() + "Using equal from Employee, the result is: " + eOne.equals(eTwo));
		System.out.println();
		
		System.out.println("---Changing the hireDate of a clone test---");
		LocalDate dDateMod = LocalDate.parse("2010-10-10");
		dCopy.setHireDate(dDateMod);
		System.out.println("This is David:\n" + d.toString() + "\nThis is a clone of David:\n" + dCopy.toString() + "Are they identical? (T/F) " + d.equals(dCopy) + "\n");
		
		System.out.println("---Declaring a Manager object as an Employee---");
		LocalDate fDate = LocalDate.parse("2011-11-11");
		Employee f = new Manager("Frank", 200000, fDate);
		System.out.println("This is Frank:\n" + f.toString());
		
		System.out.println("---Answering the Questions---");
		System.out.println("---What do you expect when you test whether an Employee is equal to a clone of the Employee?---");
		System.out.println("As shown with Alan and Brad, they are shown to be equal, and give a true value.\n");
		System.out.println("---What do you expect when you test whether a Manager is equal to an Employee with the same name and salary (and vice versa)?---");
		System.out.println("Testing a Manager as equal to an Employee, if you use the function from the Employee class, it works, as it doesn't compare anything that Manager doesn't have;\nhowever, using Manager's function, it looks for data it doesn't have, and thus fails.\n");
		System.out.println("---What do you expect when you test whether the name of an Employee is equal (or ==) to the name of a clone of the Employee?---");
		System.out.println("I would expect a true value to be given, and has been shown to be true.\n");
		System.out.println("---If you change the hire date of a clone of a Manager, is the hire date of the original Manager also changed?---");
		System.out.println("No, the hire date of the original Manager is not changed.\n");
		System.out.println("---If a Manager object is declared as an Employee, e.g. Employee e = new Manager(...), which methods of Manager can be called on this object?---");
		System.out.println("You cannot use any of them; they are undefined for the class of Employee; if there is an overlapping name, it just uses the Employee one.");
	}
}