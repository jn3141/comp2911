/**
 * Simple class to represent an employee
 * @author Justin
 * @version 1.0
 */

public class Employee implements Cloneable {
	
	private String name;
	private int salary;
	
	
	/**
	 * this is the constructor for this class
	 * it generates the object employee given a name and a salary
	 * @param name   the name of the employee
	 * @param salary the salary of the employee
	 */
	
	public Employee(String name, int salary) {
		this.name = name;
		this.salary = salary;
	}
	
	
	/**
	 * method to set an employee's name
	 * @param name the name of the employee
	 */
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * method to set an employee's salary
	 * @param salary the salary of the employee
	 */
	
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	/**
	 * method to return an employee's name
	 * @return name the name of the employee
	 */
	
	public String getName() {
		return name;
	}
	
	/**
	 * method to return an employee's salary
	 * @return salary the salary of the employee
	 */
	
	public int getSalary() {
		return salary;
	}


	@Override
	public String toString() {
		return "Name: " + name + "\n Type: " + getClass().getName() + "\n Salary: $" + salary + "\n";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Employee other = (Employee) obj;
		
		if (name == null && other.name != null) {
			return false;
		} else if (!name.equals(other.name)) {
			return false;
		}
		
		if (salary != other.salary) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public Employee clone() {
		try {
			return (Employee) super.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println (" Cloning not allowed in Employee.\n" );
			return this;
		}
	}
}