
public interface Set<E> {
	
	/**
	 * Adds an element e to the set
	 * @param e element to be added
	 */
	public void add(E e);
	
	/**
	 * Removes an element e from the set
	 * @param e element to be removed
	 */
	public void remove(E e);
	
	/**
	 * Checks if a set contains the element e
	 * @param e element to be checked
	 */
	public boolean contains(E e);
	
	/**
	 * Takes two 
	 * @param e1
	 * @param e2
	 * @return
	 */
	public Set<E> union(Set<E> e);
	
	/**
	 * @param e1
	 * @param e2
	 * @return
	 */
	public Set<E> intersect(Set<E> e);
	
	/**
	 * @param largerSet
	 * @param smallerSet
	 * @return
	 */
	public boolean subset(Set<E> smallerSet);
	
	/**
	 * @param otherSet
	 * @return
	 */
	@Override
	public boolean equals(Object otherSet);

}
