import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListSet<E> implements Set<E>, Iterable<E> {
	
	private ArrayList<E> set;
	
	public ArrayListSet() {
		set = new ArrayList<E>();
	}
	
	public void add(E e) {
		for (E element : set) {
			if (element.equals(e)) {
				return;
			}
		}
		set.add(e);
	}

	public void remove(E e) {
		for (int i = 0; i < set.size(); i++) {
			if (set.get(i).equals(e)) {
				set.remove(i);
				break;
			}
		}
	}

	public boolean contains(E e) {
		for (E element : set) {
			if (element.equals(e)) {
				return true;
			}
		}
		return false;
	}

	public Set<E> union(Set<E> e) {
		Set<E> unionSet = new ArrayListSet<E>();
		for (E element : set) {
			unionSet.add(element);
		}
		for (E element : ((ArrayListSet<E>) e).set) {
			unionSet.add(element);
		}
		return unionSet;
	}

	public Set<E> intersect(Set<E> e) {
		Set<E> interSet = new ArrayListSet<E>();
		for (E element1 : set) {
			for (E element2 : ((ArrayListSet<E>) e).set) {
				if (element1.equals(element2)) {
					interSet.add(element1);
				}
			}
		}
		return interSet;
	}

	public boolean subset(Set<E> smallerSet) {
		boolean hasElement = false;
		for (E element1 : ((ArrayListSet<E>) smallerSet).set) {
			for (E element2 : set) {
				if (element1.equals(element2)) {
					hasElement = true;
				}
			}
			if (hasElement == false) {
				return false;
			}
			hasElement = false;
		}
		return true;
	}

	@Override
	public String toString() {
		String returnString = "";
		for (E element : set) {
			returnString += element.toString();
		}
		return returnString;
	}
	
	@Override
	public Iterator<E> iterator() {
		return set.listIterator();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((set == null) ? 0 : set.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
	
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		ArrayListSet<E> other = (ArrayListSet<E>) obj;
		
		if (set == null) {
			if (other.set != null) {
				return false;
			}
		} else if (!set.equals(other.set)) {
			return false;
		}
		
		if (set.size() != other.set.size()) {
			return false;
		}
		
		for (E element : set) {
			if (!other.set.contains(element)) {
				return false;
			}
		}
		
		return true;
	}
}
