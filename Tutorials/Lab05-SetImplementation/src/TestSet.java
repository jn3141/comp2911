import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TestSet {
	public static void main(String args[]) {
		
		ArrayListSet<Integer> set1 = new ArrayListSet<Integer>();
		ArrayListSet<Integer> set2 = new ArrayListSet<Integer>();
		
		Scanner set1Input = null;
		Scanner set2Input = null;
		
		try {
			set1Input = new Scanner(new File("set1.txt"));
			while (set1Input.hasNext()) {
				set1.add(set1Input.nextInt());
			}
			
			set2Input = new Scanner(new File("set2.txt"));
			while (set2Input.hasNext()) {
				set2.add(set2Input.nextInt());
			}
			
		} catch (FileNotFoundException e) {
			
		} finally {
			if (set1Input != null) {
				set1Input.close();
			}
			
			if (set2Input != null) {
				set2Input.close();
			}
		}
		
//		ArrayListSet<Integer> newSet = (ArrayListSet) set1.union(set2);
//		System.out.println(newSet.toString());
		System.out.println(set1.subset(set2));
	}
}
