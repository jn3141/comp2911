import static org.junit.Assert.*;

import org.junit.Test;

public class InternetAccountTest {

	@Test
	public void testMakeInternetPayment() {
		InternetAccount newIA = new InternetAccount("Alvin");
		
		newIA.deposit(1000);
		
		assertFalse(newIA.makeInternetPayment(1000));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertTrue(newIA.makeInternetPayment(50));
		assertFalse(newIA.makeInternetPayment(50));
	}

}
