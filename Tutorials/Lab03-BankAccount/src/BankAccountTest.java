import static org.junit.Assert.*;

import org.junit.Test;

public class BankAccountTest {

	@Test
	public void testWithdraw() {
		BankAccount newBA = new BankAccount("Alvin");
		assertFalse(newBA.withdraw(100));
		assertEquals(newBA.getBal(), 0);
		
		newBA.deposit(900);
		assertTrue(newBA.withdraw(800));
		assertEquals(newBA.getBal(), 100);
		assertFalse(newBA.withdraw(800));
	}

	@Test
	public void testDeposit() {
		BankAccount newBA = new BankAccount("Alvin");
		assertFalse(newBA.deposit(0));
		
		assertTrue(newBA.deposit(500));
		assertEquals(newBA.getBal(), 500);
	}

}
