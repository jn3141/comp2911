import java.time.LocalDate;
import java.time.Month;

/**
 * Simple class to implement an internet account
 * It extends bank account
 * @author Justin
 */
public class InternetAccount extends BankAccount {
	private int internetMonth = 0;
	private int monthlyInternetLimit = 10;
	private Month monthOfInternetLimit;
	
	/**
	 * Constructor method for this class
	 * @param acctOwner          the name of the account owner
	 * @param balance            the balance of this account
	 * @param withdrawnToday     the amount of money withdrawn today
	 * @param dailyWithdrawLimit the daily limit of 800
	 * @param dayOfLimit         the date the withdrawnToday applies to
	 */
	public InternetAccount(String acctOwner) {
		super(acctOwner);
		this.monthOfInternetLimit = LocalDate.now().getMonth();
	}
	
	/**
	 * Pays the specified amount via the internet
	 * @param internetAmt the amount being paid via the internet
	 * @pre internetAmt > 0
	 * @pre (internetAmt + withdrawnToday) <= dailyWithdrawLimit
	 * @pre internetAmt <= balance
	 * @post balance = balance - internetAmt
	 * @post internetMonth++
	 * @return boolean whether or not it succeeded
	 */
	public boolean makeInternetPayment(int internetAmt) {
		if (canInternetPay(internetAmt)) {
			setBal(getBal() - internetAmt);
			setWithdrawnToday(getWithdrawnToday() + internetAmt);
			internetMonth++;
			System.out.println("You have successfully made the internet payment of " + internetAmt + ". Thank you, have a nice day!");
			return true;
		} else {
			System.out.println("Not enough funds for internet payment and/or surpassed withdrawal / internet limit");
			return false;
		}
	}
	
	/**
	 * Checks if the amount trying to be withdrawn can be withdrawn
	 * @param internetAmt the amount the user is attempting to pay via the internet
	 * @pre treating superclass as subclass /////////////////////
	 * @post treating subclass as superclass ///////////////////
	 * @return boolean whether or not that amount can be paid via the internet
	 */
	public boolean canInternetPay(int internetAmt) {
		updateInternetToday();
		return canWithdraw(internetAmt) && ((1 + internetMonth) <= monthlyInternetLimit);
	}
	
	/**
	 * Checks whether or not month of now is after the month of the recorded limit usage,
	 *   and update the values as necessary
	 */
	public void updateInternetToday() {
		Month newMonth = LocalDate.now().getMonth();
		if (newMonth.getValue() > monthOfInternetLimit.getValue()) {
			monthOfInternetLimit = newMonth;
			internetMonth = 0;
		}
		return;
	}
	/**
	 * Prints to console the account owner, the balance, how much of
	 *   today's limit has been drawn, and how much of this month's internet
	 *   payments have been used
	 */
	@Override
	public String toString() {
		return super.toString() + "Internet payment usage: " + internetMonth + "/10\n";
	}
}
