import java.time.LocalDate;

/**
 * Simple class to implement a bank account
 * @author Justin
 * @invariant balance >= 0
 */
public class BankAccount {
	
	private String acctOwner;
	private int balance = 0;
	private int withdrawnToday = 0;
	private int dailyWithdrawLimit = 800;
	private LocalDate dayOfWithdrawLimit;
	
	/**
	 * Constructor method for this class
	 * @param acctOwner          the name of the account owner
	 * @param balance            the balance of this account, in cents
	 * @param withdrawnToday     the amount of money withdrawn today
	 * @param dailyWithdrawLimit the daily limit of 800
	 * @param dayOfLimit         the date the withdrawnToday applies to
	 */
	public BankAccount(String acctOwner) {
		this.acctOwner = acctOwner;
		this.dayOfWithdrawLimit = LocalDate.now();
	}
	
	/**
	 * Withdraw the specified amount from the bank account
	 * @param withdrawAmt the amount being withdrawn
	 * @pre withdrawAmt > 0
	 * @pre (withdrawAmt + withdrawnToday) <= dailyWithdrawLimit
	 * @pre withdrawAmt <= balance
	 * @post balance = balance - withdrawalAmt
	 * @post withdrawnToday = withdrawnToday + withdrawAmt
	 * @return boolean whether or not it succeeded
	 */
	public boolean withdraw(int withdrawAmt) {
		if (canWithdraw(withdrawAmt)) {
			this.balance -= withdrawAmt;
			withdrawnToday += withdrawAmt;
			System.out.println("You have withdrawn " + withdrawAmt + ". Thank you, have a nice day!");
			return true;
		} else {
			System.out.println("Not enough funds to withdraw and/or surpassed withdrawal limit");
			return false;
		}
	}
	
	/**
	 * Deposit specified amount into bank account
	 * @param depositAmt the amount being deposited
	 * @pre   depositAmt > 0
	 * @post  balance = balance + depositAmt
	 * @return boolean whether or not it succeeded
	 */
	public boolean deposit(int depositAmt) {
		if (depositAmt > 0) {
			this.balance += depositAmt;
			System.out.println("You have deposited " + depositAmt + ". Thank you, have a nice day!");
			return true;
		} else {
			System.out.println("You cannot deposite a value less than 0. Please try again.");
			return false;
		}
	}
	
	/**
	 * Checks if the amount trying to be withdrawn can be withdrawn
	 * @param withdrawAmt the amount that user is attempting to withdraw
	 * @pre treating superclass as subclass /////////////////////
	 * @post treating subclass as superclass ///////////////////
	 * @return boolean whether or not that amount can be withdrawn
	 */
	public boolean canWithdraw(int withdrawAmt) {
		updateWithdrawnToday();
		return withdrawAmt > 0 && ((withdrawAmt + withdrawnToday) <= dailyWithdrawLimit) && (withdrawAmt <= balance);
	}
	
	/**
	 * Checks whether or not the date has changed, and update the withdraw
	 *   limit and the like as necessary
	 */
	public void updateWithdrawnToday() {
		LocalDate newDate = LocalDate.now();
		if (newDate.isAfter(dayOfWithdrawLimit)) {
			dayOfWithdrawLimit = newDate;
			withdrawnToday = 0;
		}
		return;
	}

	/**
	 * Gets the balance of the account
	 */
	public int getBal() {
		return this.balance;
	}
	
	/**
	 * Sets the balance of the account
	 */
	public void setBal(int balance) {
		this.balance = balance;
	}
	
	/**
	 * Gets the withdrawn today of the account
	 */
	public int getWithdrawnToday() {
		return this.withdrawnToday;
	}
	
	/**
	 * Sets the withdrawn today of the account
	 */
	public void setWithdrawnToday(int withdrawnToday) {
		this.withdrawnToday = withdrawnToday;
	}
	
	/**
	 * Prints to console the account owner, the balance, and how much of today's limit has been drawn
	 */
	@Override
	public String toString() {
		return "Account owner: " + acctOwner + "\nBalance: $" + balance + "\nWithdraw limit usage: $" + withdrawnToday + "/$800\n";
	}
}
